<?php
require_once __DIR__ . '/vendor/autoload.php';
use mywishlist\modele\Liste;
use mywishlist\modele\Item;
use mywishlist\modele\User;

use mywishlist\controleur\ControleurItem;
use mywishlist\controleur\ControleurListe;
use mywishlist\controleur\ControleurAccueil;


use Illuminate\Database\Capsule\Manager as DB;
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


$app = new \Slim\Slim;

$app->get('/', function () {
  $c=new ControleurListe();
  $c->getAccueil();
})->name('route_accueil');

// affichage de toutes les listes
$app->get('/liste', function () {
  $c=new ControleurListe();
  $c->getAllListe();
})->name('route_allliste');

//affiche de la liste avec le numero correspondant
$app->get('/liste/id/:no', function($no){
    $c=new ControleurListe();
    $c->getListeById($no);
})->name('route_liste');

//affiche de la liste ave le token
$app->get('/liste/:token', function($token){
    $c=new ControleurListe();
    $c->getListeByToken($token);
})->name('route_liste_token');

//affiche de l'item avec l'id correspondant
$app->get('/item/:id', function($id){
    $c = new ControleurItem();
    $c->getItemById($id);
})->name('route_item');

//affiche de l'item avec l'id correspondant
$app->get('/liste/:token/item/:id', function($token,$id){
    $c = new ControleurItem();
    $c->getItemIdListeToken($token,$id);
})->name('route_item_liste');

//page de creation d'une nouvelle liste
$app->get('/creationliste', function(){
    $c = new ControleurListe();
    $c->createListe();
})->name('route_create_liste');

//page de parametres d'une liste
$app->get('/liste/:token/parametres', function($token){
    $c = new ControleurListe();
    $c->getParamListe($token);
})->name('route_param_liste');

//page de modification d'un item d'une liste
$app->get('/liste/:token/item/:id/modif', function($token, $id) use ($app){
    $c=new ControleurItem();
    $c->getModifItem($id);
})->name('route_modif_item');

//page d'ajout d'un item a une liste
$app->get('/liste/:token/parametres/ajouter-item', function($token){
    $c = new ControleurListe();
    $c->addItemListe($token);
})->name('route_param_liste_add_item');

//page de parametre de la liste
$app->get('/liste/:token/parametres/modif', function($token){
    $c = new ControleurListe();
    $c->getModifListe($token);
})->name('route_param_liste_modif');


//page de parametre de la liste
$app->get('/liste/:token/parametres/supprimer-liste', function($token){
    $c = new ControleurListe();
    $c->supprListe($token);
})->name('route_param_liste_suppr');



$app->post('/creationliste', function() use ($app){
  $c=new ControleurListe();
  $c->addListe($app->request);
});

$app->post('/liste/:token/item/:id/modif', function($token, $id) use ($app){
    $c=new ControleurItem();
    $c->changeItem($token, $id, $app->request);
});

$app->post('/liste/:token/parametres/ajouter-item', function($tokenListe) use ($app){
  $c=new ControleurItem();
  $c->addItem($tokenListe, $app->request);
});

$app->post('/liste/:token/parametres/modif', function($token) use ($app){
  $c=new ControleurListe();
  $c->setModif($token, $app->request);
});

$app->post('/liste/:token/reserver-item/:id', function($token, $id) use ($app){
  $c=new ControleurItem();
  $c->reserverItem($id, $token, $app->request);
})->name('route_reservation');

$app->run();
