# s3a_baudon_foltzenlogel_frantzen_gardel

### Participants
* Nicolas BAUDON
* Raphaël FOLTZENLOGEL
* Réjane FRANTZEN
* Valentin GARDEL

### Installation
* Installer un serveur http (webetu dans ce cas), php et une base de données mysql ainsi que composer.
* Créer un fichier conf.ini correspondant à notre base de données grâce au fichier conf.ini.sample.
* Ajouter les données de test du fichier mywishlist.sql dans notre base de données.
* Lancer la commande "composer install".

### Ressources
[Dossier Drive](https://docs.google.com/document/d/1EJVfVp6Q5k_58VJ0iIw7jC9bFDhAZCYX4v_SCclJd-k/edit?usp=sharing)

[Bitbucket](https://bitbucket.org/NicolasBaudon/s3a_mywishlist_baudon_foltzenlogel_frantzen_gardel/src/master/)

[Lien vers le site](https://webetu.iutnc.univ-lorraine.fr/www/foltzenl1u/s3a_mywishlist_baudon_foltzenlogel_frantzen_gardel)