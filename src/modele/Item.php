<?php
namespace mywishlist\modele;

class Item extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'item';
  protected $primaryKey = 'id';
  public $timestamps=false;

  //Fonction permettant de faire un lien entre la liste et ses items
  public function liste() {
      return $this->belongsTo('mywishlist\model\Liste','no');
  }

  public function __toString(){
    $img="";
    if ($this->img==NULL) {
      $img="pas_image.jpg";
    }else {
      $img=$this->img;
    }
    $routeImg = str_replace ('/index.php','',$_SERVER['SCRIPT_NAME'].'/img/'.$img);
    $urlItem="";
    if ($this->url!="") {//si le createur rentre un url il serat affiche
      $urlItem="<div class='url_item'><a href='$this->url'>Vous pouvez le trouver <strong>ici</strong></a></div>";
    }
    $item="<div class='item'>
          <div class='nom_item'>$this->nom</div>
            <img id='img_item' src='$routeImg'>
          <div class='descript_item'>$this->descr</div>
          <div class='tarif_item'>$this->tarif €</div>
          $urlItem
        </div>";

      return $item;
  }
}
